/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.reto5final.dataBase;

import java.sql.*;
import java.util.logging.*;
/**
 *
 * @author JOSEPH
 */
public class SQLConection {
    private String url ="";
    public Connection con = null;
    private Statement stm = null;
    private ResultSet res = null;
    
    public SQLConection () {
    url ="jdbc:sqlite:Reto5.db";
        try {
            con = DriverManager.getConnection(url);
            if (con!=null) {
                DatabaseMetaData meta = con.getMetaData();
                createDB();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
}
    
    public Connection getConnection() {
        return con;
    }
    
    public void closeConnection(Connection con) {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(SQLConection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void cerrarConexion() {
        closeConnection(con);
    }
    
    public ResultSet consultarBD (String sentencia) {
        try {
            stm = con.createStatement();
            res = stm.executeQuery(sentencia);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (RuntimeException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ot) {
            System.out.println(ot.getMessage());
        }
        return res;
    }
    
    public boolean createDB() {
        try {
            String sentencia = "create table if not exists Productos (" +
"	id	TEXT NOT NULL," +
"	nombre	TEXT NOT NULL," +
"	temperatura	TEXT NOT NULL," +
"	valorBase	TEXT," +
"	PRIMARY KEY(id)" +
");";
            stm = con.createStatement();
            stm.execute(sentencia);
        } catch (SQLException | RuntimeException e ) {
            System.out.println("ERROR RUTINA: " + e);
            return false;
        }
        return true;
    }
    
    public boolean insertarBD(String sentencia) {
        try {
            stm = con.createStatement();
            stm.execute(sentencia);
        } catch (SQLException | RuntimeException sqlex) {
            System.out.println("ERROR RUTINA: " + sqlex);
            return false;
        }
        return true;
        
    }
    
    public boolean borrarBD(String sentencia) {
        try {
            stm = con.createStatement();
            stm.execute(sentencia);
        } catch (SQLException | RuntimeException sqlex) {
            System.out.println("ERROR RUTINA: " + sqlex);
            return false;
        }
        return true;
    }
    
    public boolean actualizarBD(String sentencia) {
        try {
            stm = con.createStatement();
            stm.executeUpdate(sentencia);
        } catch (SQLException | RuntimeException sqlex) {
            System.out.println("ERROR RUTINA: " + sqlex);
            return false;
        }
        return true;
    }
    
    public boolean setAutoCommitBD(boolean parametro) {
        try {
            con.setAutoCommit(parametro);
        } catch (SQLException sqlex) {
            System.out.println("Error al configurar el autoCommit " + sqlex.getMessage());
            return false;
        }
        return true;
    }
    
    public boolean commitBD() {
        try {
            con.commit();
            return true;
        } catch (SQLException sqlex) {
            System.out.println("Error al hacer commit " + sqlex.getMessage());
            return false;
        }
    }
    
    public boolean rollbackBD() {
        try {
            con.rollback();
            return true;
        } catch (SQLException sqlex) {
            System.out.println("Error al hacer rollback " + sqlex.getMessage());
            return false;
        }
    }
}
