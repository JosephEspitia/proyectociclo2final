/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.reto5final.models;

import com.mycompany.reto5final.dataBase.SQLConection;
import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author JOSEPH
 */
public class Productos {
    private String id;
    private String nombre;
    private double temperatura;
    private double valorBase;

    public Productos() {
    }

    public Productos(String id, String nombre, double temperatura, double valorBase) {
        this.id = id;
        this.nombre = nombre;
        this.temperatura = temperatura;
        this.valorBase = valorBase;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public double getValorBase() {
        return valorBase;
    }

    public void setValorBase(double valorBase) {
        this.valorBase = valorBase;
    }
    
    public Productos getProducto(String id) {
        SQLConection conexion = new SQLConection();
        String sql ="select * from productos WHERE id="+id+";";
        try {
            ResultSet rs = conexion.consultarBD(sql);
            if (rs.next()) {
                this.id= rs.getString("id");
                this.nombre=rs.getString("nombre");
                this.temperatura=Double.parseDouble(rs.getString("temperatura"));
                this.valorBase=Double.parseDouble(rs.getString("valorBase"));
                
            }
        } catch (SQLException e) {
            System.out.println("Error" + e.getMessage());
        } finally {
            conexion.cerrarConexion();
        }
        return this;
    }
    
    public List<Productos> listarProductos(){
        List<Productos> productList = new ArrayList<>();
        SQLConection con = new SQLConection();
        String sql = "select * from productos;";
        try {
            ResultSet rs= con.consultarBD(sql);
            Productos prod;
            while (rs.next()) {
                prod = new Productos();
                prod.setId(rs.getString("id"));
                prod.setNombre(rs.getString("nombre"));
                prod.setTemperatura(Double.parseDouble(rs.getString("temperatura")));
                prod.setValorBase(Double.parseDouble(rs.getString("valorBase")));
                productList.add(prod);
            }
        } catch (SQLException e) {
            System.out.println("Error" +e.getMessage());
        } finally {
            con.cerrarConexion();
        }
        return productList;
    }
    
    public boolean guardarProductos() {
        SQLConection con = new SQLConection();
        String sql = "INSERT INTO productos(id,nombre,temperatura,valorBase)"
                + "VALUES("+this.id+",'" + this.nombre + "','" + this.temperatura + "'," + this.valorBase + ");";
        
        if (con.setAutoCommitBD(false)) {
            if (con.insertarBD(sql)) {
                con.commitBD();
                con.cerrarConexion();
                return true;
            } else {
                con.rollbackBD();
                con.cerrarConexion();
                return false;
            }
        } else {
            con.cerrarConexion();
            return false;
        }
    }
    
     public boolean actualizarProductos() {
        SQLConection conexion = new SQLConection();
        String sql = "UPDATE productos SET nombre='"
                +  this.nombre + "',temperatura='" + this.temperatura + "',valorBase='"+ this.valorBase +"' WHERE id='" + this.id + "';";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(sql)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
     
     public boolean eliminarProductos() {
        SQLConection conexion = new SQLConection();
        String sql = "DELETE FROM productos WHERE id=" + this.id + ";";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(sql)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
}
