/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.reto5final.controlers;

import com.mycompany.reto5final.models.Productos;
import com.mycompany.reto5final.views.ViewProducts;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author JOSEPH
 */
public class ControlersProducts implements ActionListener, MouseListener {

    private ViewProducts productWindow;

    public ControlersProducts() {
        productWindow = new ViewProducts();
        productWindow.getBtnBuscar().addActionListener(this);
        productWindow.getBtnActualizar().addActionListener(this);
        productWindow.getBtnBorrar().addActionListener(this);
        productWindow.getBtnGuardar().addActionListener(this);
        productWindow.getTblProductos().addMouseListener(this);
        productWindow.setVisible(true);
        buscarProducto();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(productWindow.getBtnBuscar())) {
            if (productWindow.getTxtId().getText().isEmpty()) {
                buscarProducto();
            } else {
                buscarProducto(productWindow.getTxtId().getText());
            }
        }

        if (e.getSource().equals(productWindow.getBtnBorrar())) {
            if (!productWindow.getTxtId().getText().isEmpty()) {
                eliminarProducto(productWindow.getTxtId().getText());
            } else {
                JOptionPane.showMessageDialog(productWindow, "Digite el ID del producto.", "Error", JOptionPane.WARNING_MESSAGE);
            }
        }

        if (e.getSource().equals(productWindow.getBtnGuardar())) {
            if (productWindow.getTxtId().isEnabled()) {
                agregarProducto();
            } else {
                editarProducto();
            }
        }

        if (e.getSource().equals(productWindow.getBtnActualizar())) {
            nuevoProducto();
        }

    }

    private boolean validarRegistro() {
        boolean valido = false;
        if (!productWindow.getTxtId().getText().isEmpty()
                & !productWindow.getTxtNombre().getText().isEmpty()
                & !productWindow.getTxtTemperatura().getText().isEmpty()
                & !productWindow.getTxtValorBase().getText().isEmpty()) {
            valido = true;
        } else {
            JOptionPane.showMessageDialog(productWindow, "Digite los campor requeridos", "Campos vacios", JOptionPane.INFORMATION_MESSAGE);
        }
        return valido;
    }

    private void agregarProducto() {
        if (validarRegistro()) {
            Productos product = new Productos(productWindow.getTxtId().getText(), productWindow.getTxtNombre().getText(), Double.parseDouble(productWindow.getTxtTemperatura().getText()), Double.parseDouble(productWindow.getTxtValorBase().getText()));
            if (product.guardarProductos()) {
                JOptionPane.showMessageDialog(productWindow, "Se ha agregado el registro correctamente.", "Registro agregado", JOptionPane.INFORMATION_MESSAGE);
                nuevoProducto();
                buscarProducto();
            } else {
                JOptionPane.showMessageDialog(productWindow, "Error al guardar el registro", "Se ha producido un error al intentar guardar el registro.", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void editarProducto() {
        if (validarRegistro()) {
            Productos prod = new Productos(productWindow.getTxtId().getText(), productWindow.getTxtNombre().getText(), Double.parseDouble(productWindow.getTxtTemperatura().getText()), Double.parseDouble(productWindow.getTxtId().getText()));
            if (prod.actualizarProductos()) {
                JOptionPane.showMessageDialog(productWindow, "Se ha actualizado el registro correctamente.", "Registro actualizado", JOptionPane.INFORMATION_MESSAGE);
                nuevoProducto();
                buscarProducto();
            } else {
                JOptionPane.showMessageDialog(productWindow, "Se ha producido un error al intentar actualizar el registro.", "Error al actualizar el registro.", JOptionPane.ERROR_MESSAGE);

            }
        }
    }
    
    private void eliminarProducto(String id) {
        if(JOptionPane.showConfirmDialog(productWindow, "¿Realmente desea eliminar el registro?", "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
            if(validarRegistro())
            {
            Productos prod = new Productos(productWindow.getTxtId().getText(), productWindow.getTxtNombre().getText(), Double.parseDouble(productWindow.getTxtTemperatura().getText()), Double.parseDouble(productWindow.getTxtId().getText()));
                if( prod.eliminarProductos())
                    JOptionPane.showMessageDialog(productWindow,  "Se ha eliminado el registro correctamente.", "Registro eliminado", JOptionPane.INFORMATION_MESSAGE);
                else
                    JOptionPane.showMessageDialog(productWindow, "Se ha producido un error al intentar eliminar el registro.", "Error al eliminar el registro.", JOptionPane.ERROR_MESSAGE);            
            }
        nuevoProducto();
        buscarProducto();
        
    }
    
    private void cargarDatos(List<Productos> productox)
    {
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("id");
        modelo.addColumn("nombre");
        modelo.addColumn("temperatura");
        modelo.addColumn("valorBase");
        modelo.addColumn("costoUnitario");
        
        for(Productos item : productox)
        {
            Object[] fila = new Object[5];
            fila[0] = item.getId();
            fila[1] = item.getNombre();
            fila[2] = item.getTemperatura();
            fila[3] = item.getValorBase();
            if (item.getTemperatura() >= 0 & item.getTemperatura()<21) {
                fila[4]= item.getValorBase()*1.2;
            } else {
                fila[4]= item.getTemperatura()*1.1;
            }
            modelo.addRow(fila);
        }
        
        productWindow.getTblProductos().setModel(modelo);
    }
    
     private void cargarCampos(Productos registro)
    {
        productWindow.getTxtId().setText(registro.getId());
        productWindow.getTxtNombre().setText(registro.getNombre());
        productWindow.getTxtTemperatura().setText(String.valueOf(registro.getTemperatura()));
        productWindow.getTxtValorBase().setText(String.valueOf(registro.getValorBase()));
    }

    private void cargarCampos(Object[] registro)
    {
        productWindow.getTxtId().setText(registro[0].toString());
        productWindow.getTxtNombre().setText(registro[1].toString());
        productWindow.getTxtTemperatura().setText(registro[2].toString());
        productWindow.getTxtValorBase().setText(registro[3].toString());
    }
    
     private void buscarProducto()
    {
        Productos prod = new Productos();
        List<Productos> registrosObtenidos = prod.listarProductos();   
        cargarDatos(registrosObtenidos);
    }
    
    private void buscarProducto(String id)
    {
        Productos prod = new Productos();
        prod.setId(id);
        Productos registroObtenido = prod.getProducto(id);
        if(registroObtenido!=null)
        {
            cargarCampos(registroObtenido);
            productWindow.getTxtId().setEnabled(false);
        }
        else
            JOptionPane.showMessageDialog(productWindow, "No Encontrado", "No se ha encontrado el registro con el criterio de busqueda seleccionado.", JOptionPane.WARNING_MESSAGE);
    }
    
    private void nuevoProducto()
    {
        productWindow.getTxtId().setEnabled(true);
        productWindow.getTxtId().setText("");
        productWindow.getTxtNombre().setText("");
        productWindow.getTxtTemperatura().setText("");
        productWindow.getTxtValorBase().setText("");
        
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        JTable tabla = (JTable) e.getSource();
        if (e.getSource().equals(productWindow.getTblProductos())) {
            DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
            Vector<Object> fila = (Vector<Object>) modelo.getDataVector().elementAt(tabla.getSelectedRow());
            cargarCampos(fila.toArray());
            productWindow.getTxtId().setEnabled(false);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

}
